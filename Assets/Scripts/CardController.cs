﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class CardController : MonoBehaviour, IPointerClickHandler
{
    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Clicked: " + eventData.pointerCurrentRaycast.gameObject.name);

        AmiiboGameController.Instance.pickNextCard();

        switch (eventData.pointerCurrentRaycast.gameObject.name)
        {

            case "LV":
                AmiiboGameController.Instance.EvaluateCardFromPlayer(AmiiboGameController.Categories.LV);
                break;
            case "HP":
                AmiiboGameController.Instance.EvaluateCardFromPlayer(AmiiboGameController.Categories.HP);
                break;
            case "Attack":
                AmiiboGameController.Instance.EvaluateCardFromPlayer(AmiiboGameController.Categories.Attack);
                break;
        }
    }
}

        





