﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public GameObject myCard;

    public Queue<Amiibo> myPlayingDeck;
    public Amiibo currentCard;
   

    public void SetPlayerDeck(Amiibo[] cards)
    {
        myPlayingDeck = new Queue<Amiibo>(cards);
        
    }

    public void setNewCard()
    {
        currentCard = myPlayingDeck.Dequeue();
    }

    public void DisplayCard()
    {

        Debug.Log("called this method");
        myCard.transform.Find("Canvas").Find("PokemonName").GetComponent<Text>().text = currentCard.AmiiboName;
        myCard.transform.Find("Canvas").Find("PokemonImage").GetComponent<Image>().sprite = Resources.Load<Sprite>(currentCard.ImageName);

        myCard.GetComponent<Renderer>().material.SetTexture("_MainTex", Resources.Load<Texture>(currentCard.Cardtype));

        myCard.transform.Find("Canvas").Find("HP").GetComponent<Text>().text = "HP: " + currentCard.HP;
        myCard.transform.Find("Canvas").Find("LV").GetComponent<Text>().text = "LV: " + currentCard.LV;
        myCard.transform.Find("Canvas").Find("Attack").GetComponent<Text>().text = "Attack: " + currentCard.Attack;

    }

}