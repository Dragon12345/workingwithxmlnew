﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


 public class Dealer
{

	private Queue<Amiibo> shuffledDeck;

	public Dealer(Amiibo[] cards)
	{
		cards = ShuffleCards(cards);

		this.shuffledDeck = new Queue<Amiibo>(cards);
	}

	private Amiibo[] ShuffleCards(Amiibo[] cards)

	{

	for (int i = cards.Length - 1; i > 0; i--)

	{

     // Randomize a number between 0 and i (so that the range decreases each time)
          
     int rnd = UnityEngine.Random.Range(0, i);

            // Save the value of the current i, otherwise it'll overwrite when we swap the values


            Amiibo temp = cards[i];

     // Swap the new and old values

	cards[i] = cards[rnd];

    cards[rnd] = temp;

		}

     return cards;


	}

	public Amiibo getNextCard()

	{
		if (shuffledDeck.Count > 0) return shuffledDeck.Dequeue();

		else return null;
	}
	public bool hasCardsLeft()
	{
		if (shuffledDeck.Count > 0) return true;

        else return false;

}

 }