﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 

public class AmiiboGameController : Singleton<AmiiboGameController>
{

    public enum Categories
    {
        Attack,
        HP,
        LV,
    }
   

    private Amiibo[] amiibos;

    [SerializeField]
    private int currentCardCount = 0;

    public Player player1;
    public Player player2;


    // Start is called before the first frame update
    void Start()
    {
        amiibos = MyUtilities.LoadAmiiboXMLData("amiibos");

        Debug.Log(amiibos.Length);
        Amiibo[] p1cards = new Amiibo[6];
        Amiibo[] p2cards = new Amiibo[6];

        for (int i = 0; i < 6; i++)
        {
            p1cards[i] = amiibos[i];
        }

        for (int i = 0; i < 6; i++)
        {
            p2cards[i] = amiibos[i + 6];
        }

        player1.SetPlayerDeck(p1cards);
        player2.SetPlayerDeck(p2cards);

        StartNewTurn();

    }

    private void StartNewTurn()
    {
        player1.setNewCard();
        player2.setNewCard();

        player1.DisplayCard();
        player2.DisplayCard();
    }


    public void EvaluateCardFromPlayer(Categories category)
    {
        Amiibo[] cardsToTest = GetAllCurrentCards();
        switch (category)
        {
            case Categories.LV:
                Debug.Log("should test LV values");
                if (player1.currentCard.LV > player2.currentCard.LV)
                {
                    player1.myPlayingDeck.Enqueue(player2.currentCard);
                    player1.myPlayingDeck.Enqueue(player1.currentCard);
                }
                else
                {
                    player2.myPlayingDeck.Enqueue(player1.currentCard);
                }
                break;
                
                case Categories.HP:
                Debug.Log("should test HP values");
                if (player1.currentCard.HP > player2.currentCard.HP)

                 
                {
                    player1.myPlayingDeck.Enqueue(player2.currentCard);
                    player1.myPlayingDeck.Enqueue(player1.currentCard);
                }
                else
                {
                    player2.myPlayingDeck.Enqueue(player1.currentCard);
                }
                break;

            case Categories.Attack:
                Debug.Log("should test Attack values");
                if (player1.currentCard.Attack > player2.currentCard.Attack)
                {
                    player1.myPlayingDeck.Enqueue(player2.currentCard);
                    player1.myPlayingDeck.Enqueue(player1.currentCard);
                }
                else
                {
                    player2.myPlayingDeck.Enqueue(player1.currentCard);
                }
                break;

        }

        if (player1.myPlayingDeck.Count == 0)
        {
            SceneManager.LoadScene("Win");
            Debug.Log("win player2");

        }

        if (player2.myPlayingDeck.Count == 0)
        {
            SceneManager.LoadScene("Win2");
            Debug.Log("win Player1");
        }

        Debug.Log(player1.myPlayingDeck.Count);
        Debug.Log(player2.myPlayingDeck.Count);
        StartNewTurn();

    }
    public void EvaluateCardFromPlayer(string topic)
    {
        Debug.Log(topic);
        if (topic == "Attack")
        {
            if (player1.currentCard.Attack > player2.currentCard.Attack)
            {

                player1.myPlayingDeck.Enqueue(player1.currentCard);


                SceneManager.LoadScene("Win");
                Debug.Log("win Player1");
            }

            else
            {
                player2.myPlayingDeck.Enqueue(player2.currentCard);
                SceneManager.LoadScene("Win2");
                Debug.Log("win Player2");
            }
 
        }

        else if (topic == "HP")
        {
            if (player1.currentCard.HP > player2.currentCard.HP)
            {

                player1.myPlayingDeck.Enqueue(player1.currentCard);
                SceneManager.LoadScene("Win");
                Debug.Log("win player1");
            }

            else

            {
                player2.myPlayingDeck.Enqueue(player2.currentCard);
                SceneManager.LoadScene("Win2");
                Debug.Log("win player2");
            }

            
        }

        else if (topic == "LV")
        {
            if (player1.currentCard.LV > player2.currentCard.LV)
            {

                player1.myPlayingDeck.Enqueue(player1.currentCard);
                SceneManager.LoadScene("Win");
                Debug.Log("win player1");
            }

            else

            {
                player2.myPlayingDeck.Enqueue(player2.currentCard);
                SceneManager.LoadScene("Win2");
                Debug.Log("win player2");
            }
        }



        if (player1.myPlayingDeck.Count == 0)
        {
            SceneManager.LoadScene("Win");
            Debug.Log("win player2");

        }

        if (player2.myPlayingDeck.Count == 0)
        {
            SceneManager.LoadScene("Win2");
            Debug.Log("win Player1");
        }

        Debug.Log(player1.myPlayingDeck.Count);
        Debug.Log(player2.myPlayingDeck.Count);
        StartNewTurn();
    }

    public void pickNextCard()
    {
        currentCardCount++;
        if (currentCardCount == amiibos.Length) currentCardCount = 0;

        /*private void DisplayCard(currentCardCount);*/
    }


    private Amiibo[] GetAllCurrentCards()
    {
        return new Amiibo[4];
    }
}


