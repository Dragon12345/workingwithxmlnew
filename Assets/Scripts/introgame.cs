﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;





public class introgame : Singleton<introgame>
{
    private Amiibo[] amiibos;

    [SerializeField]


    private int currentCardCount = 0;

    // Start is called before the first frame update


    void Start()

    {
        amiibos = MyUtilities.LoadAmiiboXMLData("amiibos");

        DisplayCard(currentCardCount);

    }

    public void pickNextCard()


    {
        currentCardCount++;

        if (currentCardCount == amiibos.Length) currentCardCount = 0;

        DisplayCard(currentCardCount);

    }

    private void DisplayCard(int currentCardCount)
    {

        GameObject.Find("PokemonName").GetComponent<Text>().text = amiibos[currentCardCount].AmiiboName;

        GameObject.Find("PokemonImage").GetComponent<Image>().sprite = Resources.Load<Sprite>(amiibos[currentCardCount].ImageName);

        GameObject.Find("Card").GetComponent<Renderer>().material.SetTexture("_MainTex", Resources.Load<Texture>(amiibos[currentCardCount].Cardtype));

        GameObject.Find("HP").GetComponent<Text>().text = "HP:" + amiibos[currentCardCount].HP;

        GameObject.Find("LV").GetComponent<Text>().text = "LV:" + amiibos[currentCardCount].LV;

        GameObject.Find("Attack").GetComponent<Text>().text = "Attack:" + amiibos[currentCardCount].Attack;
    }
}
        

        