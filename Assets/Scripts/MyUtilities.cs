using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections.Generic;


public class MyUtilities
{    

 

	public static Amiibo[] LoadAmiiboXMLData(string filename)
	{
		List<Amiibo> cards = new List<Amiibo>();

		string amiiboName = "";
		string imageName = "";
		int ssbNumber = 0;
		int versions = 0;
        int Attack = 0;
        string cardtype = "";

		TextAsset xmlData = Resources.Load(filename) as TextAsset;
		string xmlString = xmlData.text;

		Amiibo session = null;

		// Create an XmlReader
		using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
		{
			while (reader.Read())
			{

				if (reader.LocalName == "amiibo" && reader.IsStartElement())
				{
					session = null;
				}
				if (reader.IsStartElement())
				{
                    Debug.Log(reader.LocalName);
					switch (reader.LocalName)
					{
						case "name":
							amiiboName = reader.ReadElementContentAsString();
							break;
						case "image":
							imageName = reader.ReadElementContentAsString();
							break;
						case "HP":
							ssbNumber = reader.ReadElementContentAsInt();
							break;
						case "LV":
							versions = reader.ReadElementContentAsInt();
							break;
                        case "Attack":
                            Attack = reader.ReadElementContentAsInt();
                            break;
                        case "cardtype":
                            cardtype = reader.ReadElementContentAsString();
                            break;

                    }
				}
				if (reader.LocalName == "amiibo" && !reader.IsStartElement())
				{
                 
					session = new Amiibo(amiiboName,
									imageName,
									ssbNumber,
									versions,
                                     Attack,
                                     cardtype);
					cards.Add(session);
				}
			}
		}

		return cards.ToArray();
	}

}
