﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Amiibo 
{
    private string PokemonName;
    private string imageName;
    private string cardtype;
    private int hp;
    private int lv;
    private int attack;
    internal Sprite AmiiboSprite;

    public string AmiiboName { get => PokemonName; }
    public string ImageName { get => imageName; }

    public string Cardtype { get => cardtype; }
    public int HP { get => hp; }
    public int LV { get => lv; }

    public int Attack { get => attack; }
   

    public Amiibo(string PokemonName, string imageName, int HP, int LV, int attack, string cardtype)
    {
        this.PokemonName = PokemonName;
        this.imageName = imageName;
        this.cardtype = cardtype;
        this.hp = HP;
        this.lv = LV;
        this.attack = attack;

        Debug.Log(Cardtype);
        Debug.Log(AmiiboName);
    }
}
