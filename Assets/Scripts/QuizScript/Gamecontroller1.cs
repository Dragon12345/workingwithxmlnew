﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameController1 : Singleton<GameController1>
{
    private Amiibo[] amiibos;
    private Amiibo currentAmiibo;

    private int counter = 0;
    private int score = 0;

    public Image amiiboImageHolder;
    public Text[] answers = new Text[3];
    public Text scoreBox;


    void Start()
    {

        amiibos = MyUtilities.LoadAmiiboXMLData("Amiibo");
        selectNextAmiibo();
    }

    private void selectNextAmiibo()
    {
        currentAmiibo = amiibos[Random.Range(0, amiibos.Length)];

        setCanvasElements();
    }

    public void checkAnswer(int selected)
    {
        if (answers[selected].text.CompareTo(currentAmiibo.AmiiboName) == 0)
        {
            score++;
        }
        counter++;
        selectNextAmiibo();
    }

    private void setCanvasElements()
    {
        addRandomNamesToAnswerButtons();
        amiiboImageHolder.sprite = currentAmiibo.AmiiboSprite;
        int correctAnswer = Random.Range(0, 2);
        answers[correctAnswer].text = currentAmiibo.AmiiboName;
        scoreBox.text = "Score " + score + "/" + counter;
    }

    private void addRandomNamesToAnswerButtons()
    {
        foreach (Text answer in answers)
        {
            answer.text = amiibos[Random.Range(0, amiibos.Length)].AmiiboName;
        }
    }

}
